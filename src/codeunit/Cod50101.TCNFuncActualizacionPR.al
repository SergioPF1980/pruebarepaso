codeunit 50101 "TCN_FuncActualizacionPR"
{
    Subtype = Upgrade;

    trigger OnUpgradePerCompany()
    var
        culTCN_FuncionesRepaso: Codeunit TCN_FuncionesRepaso;
    begin
        culTCN_FuncionesRepaso.ActualizarIdUnicoSalesLinesF();
    end;


}