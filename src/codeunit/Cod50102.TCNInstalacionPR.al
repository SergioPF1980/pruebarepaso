codeunit 50102 "TCNInstalacionPR"
{
    Subtype = Install;

    trigger OnInstallAppPerCompany()
    var
        culTCN_FuncionesRepaso: Codeunit TCN_FuncionesRepaso;
    begin
        culTCN_FuncionesRepaso.ActualizarIdUnicoSalesLinesF();
    end;
}