codeunit 50103 "TCN_FuncionesRepaso"
{
    procedure ActualizarIdUnicoSalesLinesF()
    var
        rlSalesLine: Record "Sales Line";
        xlGuidNulo: Guid;

    begin
        rlSalesLine.SetRange(TCN_IdUnico, xlGuidNulo);
        if rlSalesLine.FindSet(true) then begin
            repeat
                rlSalesLine.AsignarIdUnicoF();
                rlSalesLine.Modify(true);
            until rlSalesLine.Next() = 0;
        end;
    end;
}