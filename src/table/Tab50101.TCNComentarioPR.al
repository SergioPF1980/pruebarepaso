table 50101 "TCN_ComentarioPR"
{
    DataClassification = ToBeClassified;
    Caption = 'Comentarios';

    fields
    {
        field(1; IdUnicoOrigen; Guid)
        {
            Caption = 'Id Unico Origen';
            DataClassification = ToBeClassified;
            Editable = false;
        }
        field(2; NumLinea; Integer)
        {
            Caption = 'Nº Linea';
            DataClassification = ToBeClassified;
        }
        field(3; Comentarios; Text[250])
        {
            Caption = 'Comentarios';
            DataClassification = ToBeClassified;

        }
    }

    keys
    {
        key(PK; IdUnicoOrigen, NumLinea)
        {
            Clustered = true;
        }
    }


}