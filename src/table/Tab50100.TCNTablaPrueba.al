table 50100 "TCN_TablaPrueba"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; Id; Integer)
        {
            DataClassification = ToBeClassified;

        }
        field(2; Nombre; Text[50])
        {
            DataClassification = ToBeClassified;

        }
        field(3; IdUnico; Guid)
        {
            DataClassification = ToBeClassified;
            Editable = false;
        }
    }

    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    trigger OnInsert()
    begin
        Validate(IdUnico, CreateGuid());
    end;

}