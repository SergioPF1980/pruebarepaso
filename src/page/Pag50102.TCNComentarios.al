page 50102 "TCN_Comentarios"
{
    Caption = 'Comentarios';
    PageType = List;
    SourceTable = TCN_ComentarioPR;
    UsageCategory = None;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Comentarios; Comentarios)
                {
                    ApplicationArea = All;
                }
                field(IdUnicoOrigen; IdUnicoOrigen)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field(NumLinea; NumLinea)
                {
                    ApplicationArea = All;
                    Visible = false;
                }
            }
        }
    }

}
