page 50100 "TCN_Prueba01"
{

    ApplicationArea = All;
    Caption = 'TCN_Prueba01';
    PageType = List;
    SourceTable = TCN_TablaPrueba;
    UsageCategory = Lists;
    CardPageId = TCN_Prueba02;


    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(IdUnico; IdUnico)
                {
                    ApplicationArea = All;
                }
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
                field(Id; Id)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
