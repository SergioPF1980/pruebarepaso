page 50101 "TCN_Prueba02"
{

    Caption = 'TCN_Prueba02';
    PageType = Card;
    SourceTable = TCN_TablaPrueba;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Id; Id)
                {
                    ApplicationArea = All;
                }
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
            }
            group(Otros)
            {
                field(IdUnico; IdUnico)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
